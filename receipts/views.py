from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
        context = {
            "form": form,
        }
        return render(request, "accounts/create.html", context)


@login_required
def create_expense(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
        context = {
            "form": form,
        }
        return render(request, "category/create.html", context)


def account_list(request):
    receipt_acc = Account.objects.filter(owner=request.user)
    context = {"account": receipt_acc}
    return render(request, "accounts/account_list.html", context)


def category_list(request):
    expense_cat = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category": expense_cat}
    return render(request, "category/category.html", context)


@login_required
def receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt_list}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        context = {
            "form": form,
        }
        return render(request, "receipts/create.html", context)
